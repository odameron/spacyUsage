# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 15:25:51 2021

@author: ebecker
"""

import string
import spacy
from spacy.matcher import Matcher
from spacy.tokens import Span
nlp = spacy.load('en_core_web_sm')

##############

matcher = Matcher(nlp.vocab)

pattern = [{'LEMMA': {'IN': ['my small cat', 'brisby']}},
           {'IS_ASCII': True, 'OP': '*'},
           {'LEMMA': {'IN':  ['eat', 'drink']}},
           {'IS_ASCII': True, 'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','cat food']}}]
matcher.add('Animal0Food4_cat', [pattern])

pattern = [{'LEMMA': {'IN': ['snoopy', 'the dog']}},
           {'IS_ASCII': True, 'OP': '*'},
           {'LEMMA': {'IN': ['eat', 'drink']}}, 
           {'IS_ASCII': True, 'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','cat food']}}]
matcher.add('Animal0Food4_dog', [pattern])


text = 'My small cat drinks milk. Brisby drinks milk. Snoopy drinks often water. The dog does not drink milk'
sentences = text.split('.')

for sentence in sentences :
  doc = nlp(sentence.strip().lower())
  matches = matcher(doc)
  if len(matches) == 0:
    print('NO MATCH :', sentence)
  for match_id, start, end in matches:
    matched_span = doc[start:end]
    print(nlp.vocab.strings[match_id], matched_span.text)

