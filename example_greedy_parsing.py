# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 22:52:36 2021

@author: odameron
"""

import string
import spacy
from spacy.matcher import Matcher
from spacy.tokens import Span
nlp = spacy.load('en_core_web_sm')

##############

matcher = Matcher(nlp.vocab)

pattern = [{'LEMMA': {'IN': ['white', 'black', 'red', 'green', 'blue']}},
           {'IS_ASCII': True, 'OP': '*'},
           {'LEMMA': {'IN':  ['cat', 'dog', 'yeast']}}
          ]
# NOT GREEDY
#matcher.add('ColorAnimal', [pattern])
#matcher.add('ColorAnimal', [pattern], greedy='FIRST')
# GREEDY
matcher.add('ColorAnimal', [pattern], greedy='LONGEST')

text = 'The black big cat and the tired white dog.'
sentences = text.split('.')

for sentence in sentences :
  doc = nlp(sentence.strip().lower())
  matches = matcher(doc)
  if len(matches) == 0:
    print('NO MATCH :', sentence)
  for match_id, start, end in matches:
    matched_span = doc[start:end]
    print(nlp.vocab.strings[match_id], matched_span.text)

