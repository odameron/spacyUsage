# installation

## Useful links

- https://spacy.io/
- rule-based matching: https://spacy.io/usage/rule-based-matching


## From a terminal

```bash
python3 -m venv .env
source .env/bin/activate
pip3 install -U pip setuptools wheel
pip3 install -U spacy
# one of the next two lines
python3 -m spacy download en_core_web_sm	# efficiency
#python3 -m spacy download en_core_web_trf	# accuracy
python3 -m spacy download en_core_web_md
python3 -m spacy download en_core_web_lg
```

Notes:

- `python -m venv .env` creates a virtuel environment
- `source .env/bin/activate` activates the virtuel environment
- the two following lines install spacy and its dependencies
- `python -m spacy download en_core_web_???` loads English tokenizer, tagger, parser and NER


# usage

## 1. From a terminal

```bash
cd 
source .env/bin/activate
python3
```

## 2. From the python3 console launched at step 1

### Example 1: decompose a text into sentences

```python
import spacy
nlp = spacy.load("en_core_web_sm")
nlp = spacy.load("en_core_web_md")
nlp = spacy.load("en_core_web_lg")
#nlp = spacy.load('en_core_web_trf')
nlp.add_pipe('sentencizer')
myText = "This is my first sentence. And this is the second. Eventually, the string ends with the third sentence"
doc = nlp(myText)
for sent in doc.sents:
    print(sent)
```


### Example 2: look for patterns

cf https://spacy.io/usage/rule-based-matching

```python
import spacy
from spacy.matcher import Matcher

nlp = spacy.load("en_core_web_sm")
#nlp = spacy.load('en_core_web_trf')
matcher = Matcher(nlp.vocab)
# Add match ID "HelloWorld" with no callback and one pattern
pattern = [{"LOWER": "hello"}, {"IS_PUNCT": True}, {"LOWER": "world"}]
matcher.add("HelloWorld", [pattern])
doc = nlp("Hello, world! Hello world!")
matches = matcher(doc)
for match_id, start, end in matches:
    string_id = nlp.vocab.strings[match_id]  # Get string representation
    span = doc[start:end]  # The matched span
    print(match_id, string_id, start, end, span.text)
```


