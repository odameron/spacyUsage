# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 15:25:51 2021

@author: ebecker
"""

import string
import spacy
from spacy.matcher import Matcher
from spacy.tokens import Span
nlp = spacy.load('en_core_web_sm')

##############

matcher = Matcher(nlp.vocab)

pattern = [{'LEMMA': {'IN': ['cat', 'brisby']}},
           {'OP': '*'},
           {'LEMMA': {'IN':  ['eat', 'drink']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_cat', [pattern])

pattern = [{'LEMMA': {'IN': ['snoopy', 'dog']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['eat', 'drink']}}, 
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_dog', [pattern])


text = 'My small cat drinks milk. Brisby drinks milk. Snoopy drinks often water with sugar.'
sentences = text.split('.')

print('***JUST TO SEE THE ALIGNMENTS***')
for sentence in sentences :
  doc = nlp(sentence.strip().lower())

  matches = matcher(doc, with_alignments = True)
  for match_id, start, end, aligns in matches:
    matched_span = doc[start:end]
    print(nlp.vocab.strings[match_id], matched_span.text, aligns)
    
##############

matcher = Matcher(nlp.vocab)

pattern = [{'LEMMA': {'IN': ['cat', 'brisby']}},
           {'OP': '*'},
           {'LEMMA': {'IN':  ['eat', 'drink']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_cat', [pattern])

pattern = [{'LEMMA': {'IN': ['snoopy', 'dog']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['eat', 'drink']}}, 
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_dog', [pattern])


text = 'My small cat drinks milk. Brisby drinks milk. Snoopy drinks often water with sugar.'
sentences = text.split('.')

print('\n***JUST TO SEE THE TOKENS***')
for sentence in sentences :
  doc = nlp(sentence.strip().lower())

  matches = matcher(doc, with_alignments = True)
  for match_id, start, end, aligns in matches:
    matched_span = doc[start:end]
    toks = matched_span.text.split()
    print(nlp.vocab.strings[match_id], matched_span.text, toks)
    
    
##############
    
print('\n***JUST TO GET THE POSITIONS OF ANIMALKS AND FOOD IN THE NAMES***')

def get_animal_pos(txt):
  txt = eval(txt.split('Animal')[1].split('Food')[0])
  return(txt)
  
get_animal_pos('Animal0Food4_dog')
get_animal_pos('Animal3Food4_dog')
get_animal_pos('Animal18Food4_dog')


def get_food_pos(txt):
  txt = eval(txt.split('Food')[1].split('_')[0])
  return(txt)
  
get_food_pos('Animal0Food4_dog')
get_food_pos('Animal3Food0_dog')
get_food_pos('Animal18Food6_dog')


##############

matcher = Matcher(nlp.vocab)

pattern = [{'LEMMA': {'IN': ['cat', 'brisby']}},
           {'OP': '*'},
           {'LEMMA': {'IN':  ['eat', 'drink']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_cat', [pattern])

pattern = [{'LEMMA': {'IN': ['snoopy', 'dog']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['eat', 'drink']}}, 
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_dog', [pattern])


text = 'My small cat drinks milk. Brisby drinks milk. Snoopy drinks often water with sugar.'
sentences = text.split('.')

print('***THE INFORMATION EXTRACTED***')
for sentence in sentences :
  doc = nlp(sentence.strip().lower())

  matches = matcher(doc, with_alignments = True)
  for match_id, start, end, aligns in matches:

    matched_span = doc[start:end]
    toks = matched_span.text.split()

    pat = nlp.vocab.strings[match_id]

    animal = toks[aligns.index(get_animal_pos(pat))]
    food = toks[aligns.index(get_food_pos(pat))] 

    print(nlp.vocab.strings[match_id], matched_span.text, "Animal: ", animal, "Food: ", food)
     
    
    
    
    
    
    