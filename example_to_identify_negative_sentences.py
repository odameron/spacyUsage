# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 15:25:51 2021

@author: ebecker
"""

import string
import spacy
from spacy.matcher import Matcher
from spacy.tokens import Span
nlp = spacy.load('en_core_web_sm')

##############

matcher = Matcher(nlp.vocab)

pattern = [{'LEMMA': {'IN': ['cat', 'brisby']}},
           {'OP': '*'},
           {'LEMMA': {'IN':  ['eat', 'drink']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_cat', [pattern])

pattern = [{'LEMMA': {'IN': ['snoopy', 'dog']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['eat', 'drink']}}, 
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_dog', [pattern])


text = 'My small cat drinks milk. Brisby drinks milk. Snoopy drinks often water. The dog does not drink milk but prefers water. Snoopy eats catfood and brisby is not happy. Snoopy never eats catfood.'
sentences = text.split('.')

print("***BEFORE***")
for sentence in sentences :
  doc = nlp(sentence.strip().lower())
  matches = matcher(doc)
  for match_id, start, end in matches:
    matched_span = doc[start:end]
    print(nlp.vocab.strings[match_id], matched_span.text)



##############

matcher = Matcher(nlp.vocab)

pattern = [{'LEMMA': {'IN': ['cat', 'brisby']}},
           {'OP': '*'},
           {'LEMMA': {'IN':  ['eat', 'drink']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_cat', [pattern])

pattern = [{'LEMMA': {'IN': ['snoopy', 'dog']}},
           {'OP': '*'},
           {'LEMMA': {'IN': ['eat', 'drink']}}, 
           {'OP': '*'},
           {'LEMMA': {'IN': ['milk','water','catfood']}}]
matcher.add('Animal0Food4_dog', [pattern])


text = 'My small cat drinks milk. Brisby drinks milk. Snoopy drinks often water. The dog does not drink milk but prefers water. Snoopy eats catfood and brisby is not happy. Snoopy never eats catfood.'
sentences = text.split('.')

print("***AFTER***")
for sentence in sentences :
  doc = nlp(sentence.strip().lower())
  neg = False
  for token in doc :
    if(token.dep_=="neg"):
      print("NEGATIVE SENTENCE :", sentence)
      neg = True
  
  if not(neg) :
    matches = matcher(doc)
    for match_id, start, end in matches:
      matched_span = doc[start:end]
      print(nlp.vocab.strings[match_id], matched_span.text)